import { combineReducers } from 'redux';

import actionsReducer from './actions';
import servicesReducer from './services';
import subscriptionsReducer from './subscriptions';
import dealersReducer from './dealers';

export const rootReducer = combineReducers({
  actions: actionsReducer,
  services: servicesReducer,
  subscriptions: subscriptionsReducer,
  dealers: dealersReducer,
});

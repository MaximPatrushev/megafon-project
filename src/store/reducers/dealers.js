import * as actionTypes from '../actions/actionTypes';

const initialState = {
  dealers: [],
  dealer: {},
  isDealersLoaded: false,
  validErrors: null,
};

const getDealersRequest = state => {
  return {
    ...state,
    validErrors: null,
    isDealersLoaded: false,
  };
};

const getDealersSuccess = (state, action) => {
  return {
    ...state,
    validErrors: null,
    dealers: action.dealers,
    isDealersLoaded: true,
  };
};

const getDealersFail = (state, action) => {
  return {
    ...state,
    validErrors: action.errors,
    isDealersLoaded: true,
  };
};

const getDealerByIdRequest = state => {
  return {
    ...state,
    validErrors: null,
    isDealersLoaded: false,
  };
};

const getDealerByIdSuccess = (state, action) => {
  return {
    ...state,
    validErrors: null,
    dealer: action.dealer,
    isDealersLoaded: true,
  };
};

const getDealerByIdFail = (state, action) => {
  return {
    ...state,
    validErrors: action.errors,
    isDealersLoaded: true,
  };
};

const dealersReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_DEALERS_REQUEST:
      return getDealersRequest(state, action);
    case actionTypes.GET_DEALERS_SUCCESS:
      return getDealersSuccess(state, action);
    case actionTypes.GET_DEALERS_FAIL:
      return getDealersFail(state, action);
    case actionTypes.GET_DEALER_BY_ID_REQUEST:
      return getDealerByIdRequest(state, action);
    case actionTypes.GET_DEALER_BY_ID_SUCCESS:
      return getDealerByIdSuccess(state, action);
    case actionTypes.GET_DEALER_BY_ID_FAIL:
      return getDealerByIdFail(state, action);
    default:
      return state;
  }
};

export default dealersReducer;

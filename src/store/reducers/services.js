import * as actionTypes from '../actions/actionTypes';

const initialState = {
  services: [],
  service: {},
  isServicesLoaded: false,
  validErrors: null,
};

const getServicesRequest = state => {
  return {
    ...state,
    validErrors: null,
    isServicesLoaded: false,
  };
};

const getServicesSuccess = (state, action) => {
  return {
    ...state,
    validErrors: null,
    services: action.services,
    isServicesLoaded: true,
  };
};

const getServicesFail = (state, action) => {
  return {
    ...state,
    validErrors: action.errors,
    isServicesLoaded: true,
  };
};

const getServiceByIdRequest = state => {
  return {
    ...state,
    validErrors: null,
    isServicesLoaded: false,
  };
};

const getServiceByIdSuccess = (state, action) => {
  return {
    ...state,
    validErrors: null,
    service: action.service,
    isServicesLoaded: true,
  };
};

const getServiceByIdFail = (state, action) => {
  return {
    ...state,
    validErrors: action.errors,
    isServicesLoaded: true,
  };
};

const servicesReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_SERVICES_REQUEST:
      return getServicesRequest(state, action);
    case actionTypes.GET_SERVICES_SUCCESS:
      return getServicesSuccess(state, action);
    case actionTypes.GET_SERVICES_FAIL:
      return getServicesFail(state, action);
    case actionTypes.GET_SERVICE_BY_ID_REQUEST:
      return getServiceByIdRequest(state, action);
    case actionTypes.GET_SERVICE_BY_ID_SUCCESS:
      return getServiceByIdSuccess(state, action);
    case actionTypes.GET_SERVICE_BY_ID_FAIL:
      return getServiceByIdFail(state, action);
    default:
      return state;
  }
};

export default servicesReducer;

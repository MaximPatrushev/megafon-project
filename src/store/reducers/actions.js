import * as actionTypes from '../actions/actionTypes';

const initialState = {
  actions: [],
  action: {},
  isActionLoaded: false,
  isActionsLoaded: false,
  validErrors: null,
  isLoading: false,
  isActionAdded: false,
  isActionUpdated: false,
  isActionDeleted: false,
};

const getActionsRequest = state => {
  return {
    ...state,
    validErrors: null,
    isActionsLoaded: false,
    isActionAdded: false,
    isActionUpdated: false,
    isActionDeleted: false,
  };
};

const getActionsSuccess = (state, action) => {
  return {
    ...state,
    validErrors: null,
    actions: action.actions,
    isActionsLoaded: true,
  };
};

const getActionsFail = (state, action) => {
  return {
    ...state,
    validErrors: action.errors,
    isActionsLoaded: true,
  };
};

const getActionByIdRequest = state => {
  return {
    ...state,
    validErrors: null,
    isActionLoaded: false,
    isActionDeleted: false,
    isActionUpdated: false,
  };
};

const getActionByIdSuccess = (state, action) => {
  return {
    ...state,
    validErrors: null,
    action: action.action,
    isActionLoaded: true,
  };
};

const getActionByIdFail = (state, action) => {
  return {
    ...state,
    validErrors: action.errors,
    isActionLoaded: true,
  };
};

const addActionRequest = state => {
  return {
    ...state,
    validErrors: null,
    isLoading: true,
    isActionAdded: false,
  };
};

const addActionSuccess = (state, action) => {
  return {
    ...state,
    validErrors: null,
    isLoading: false,
    isActionAdded: true,
    actions: [...state.actions, action.action],
  };
};

const addActionFail = (state, action) => {
  return {
    ...state,
    validErrors: action.errors,
    isLoading: false,
    isActionAdded: false,
  };
};

const updateActionRequest = state => {
  return {
    ...state,
    validErrors: null,
    isLoading: true,
  };
};

const updateActionSuccess = (state, action) => {
  return {
    ...state,
    validErrors: null,
    action: action.action,
    isLoading: false,
    isActionUpdated: true,
  };
};

const updateActionFail = (state, action) => {
  return {
    ...state,
    validErrors: action.errors,
    isLoading: false,
  };
};

const deleteActionRequest = state => {
  return {
    ...state,
    validErrors: null,
    isLoading: true,
  };
};

const deleteActionSuccess = (state, action) => {
  return {
    ...state,
    validErrors: null,
    isLoading: false,
    isActionDeleted: true,
  };
};

const deleteActionFail = (state, action) => {
  return {
    ...state,
    validErrors: action.errors,
    isLoading: false,
  };
};

const actionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ACTIONS_REQUEST:
      return getActionsRequest(state, action);
    case actionTypes.GET_ACTIONS_SUCCESS:
      return getActionsSuccess(state, action);
    case actionTypes.GET_ACTIONS_FAIL:
      return getActionsFail(state, action);
    case actionTypes.GET_ACTION_BY_ID_REQUEST:
      return getActionByIdRequest(state, action);
    case actionTypes.GET_ACTION_BY_ID_SUCCESS:
      return getActionByIdSuccess(state, action);
    case actionTypes.GET_ACTION_BY_ID_FAIL:
      return getActionByIdFail(state, action);
    case actionTypes.ADD_ACTION_REQUEST:
      return addActionRequest(state, action);
    case actionTypes.ADD_ACTION_SUCCESS:
      return addActionSuccess(state, action);
    case actionTypes.ADD_ACTION_FAIL:
      return addActionFail(state, action);
    case actionTypes.UPDATE_ACTION_REQUEST:
      return updateActionRequest(state, action);
    case actionTypes.UPDATE_ACTION_SUCCESS:
      return updateActionSuccess(state, action);
    case actionTypes.UPDATE_ACTION_FAIL:
      return updateActionFail(state, action);
    case actionTypes.DELETE_ACTION_REQUEST:
      return deleteActionRequest(state, action);
    case actionTypes.DELETE_ACTION_SUCCESS:
      return deleteActionSuccess(state, action);
    case actionTypes.DELETE_ACTION_FAIL:
      return deleteActionFail(state, action);
    default:
      return state;
  }
};

export default actionsReducer;

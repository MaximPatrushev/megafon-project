import * as actionTypes from '../actions/actionTypes';

const initialState = {
  subscriptions: [],
  subscription: {},
  isSubscriptionsLoaded: false,
  validErrors: null,
};

const getSubscriptionsRequest = state => {
  return {
    ...state,
    validErrors: null,
    isSubscriptionsLoaded: false,
  };
};

const getSubscriptionsSuccess = (state, action) => {
  return {
    ...state,
    validErrors: null,
    subscriptions: action.subscriptions,
    isSubscriptionsLoaded: true,
  };
};

const getSubscriptionsFail = (state, action) => {
  return {
    ...state,
    validErrors: action.errors,
    isSubscriptionsLoaded: true,
  };
};

const getSubscriptionByIdRequest = state => {
  return {
    ...state,
    validErrors: null,
    isSubscriptionsLoaded: false,
  };
};

const getSubscriptionByIdSuccess = (state, action) => {
  return {
    ...state,
    validErrors: null,
    subscription: action.subscription,
    isSubscriptionsLoaded: true,
  };
};

const getSubscriptionByIdFail = (state, action) => {
  return {
    ...state,
    validErrors: action.errors,
    isSubscriptionsLoaded: true,
  };
};

const subscriptionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_SUBSCRIPTIONS_REQUEST:
      return getSubscriptionsRequest(state, action);
    case actionTypes.GET_SUBSCRIPTIONS_SUCCESS:
      return getSubscriptionsSuccess(state, action);
    case actionTypes.GET_SUBSCRIPTIONS_FAIL:
      return getSubscriptionsFail(state, action);
    case actionTypes.GET_SUBSCRIPTION_BY_ID_REQUEST:
      return getSubscriptionByIdRequest(state, action);
    case actionTypes.GET_SUBSCRIPTION_BY_ID_SUCCESS:
      return getSubscriptionByIdSuccess(state, action);
    case actionTypes.GET_SUBSCRIPTION_BY_ID_FAIL:
      return getSubscriptionByIdFail(state, action);
    default:
      return state;
  }
};

export default subscriptionsReducer;

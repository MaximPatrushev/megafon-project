import { createSelector } from 'reselect';

const getAction = state => state.action;
const getServices = state => state.services;
const getSubscriptions = state => state.subscriptions;

// get services filtered by action
export const getFilteredServices = createSelector(
  [getAction, getServices],
  (action, services) => {
    if (action.services) {
      const idArray = action.services;
      return services.filter(service => idArray.indexOf(service.id) !== -1);
    }
  },
);

// get services filtered by action
export const getFilteredConnectedServices = createSelector(
  [getAction, getServices],
  (action, services) => {
    if (action.connected_services) {
      const idArray = action.connected_services;
      return services.filter(service => idArray.indexOf(service.id) !== -1);
    }
  },
);

// get subscriptions filtered by action
export const getFilteredSubscriptions = createSelector(
  [getAction, getSubscriptions],
  (action, subscriptions) => {
    if (action.subscriptions) {
      const idArray = action.subscriptions;
      return subscriptions.filter(
        subscription => idArray.indexOf(subscription.id) !== -1,
      );
    }
  },
);

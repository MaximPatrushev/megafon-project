import axios from '../../config/axios/axios';
import * as actionTypes from './actionTypes';

const getServicesRequest = () => {
  return {
    type: actionTypes.GET_SERVICES_REQUEST,
  };
};

const getServicesSuccess = services => {
  return {
    type: actionTypes.GET_SERVICES_SUCCESS,
    services,
  };
};

const getServicesFail = errors => {
  return {
    type: actionTypes.GET_SERVICES_FAIL,
    errors,
  };
};

const getServiceByIdRequest = () => {
  return {
    type: actionTypes.GET_SERVICE_BY_ID_REQUEST,
  };
};

const getServiceByIdSuccess = service => {
  return {
    type: actionTypes.GET_SERVICE_BY_ID_SUCCESS,
    service,
  };
};

const getServiceByIdFail = errors => {
  return {
    type: actionTypes.GET_SERVICE_BY_ID_FAIL,
    errors,
  };
};

export const getServices = () => {
  return dispatch => {
    dispatch(getServicesRequest());
    axios
      .get('/services/')
      .then(response => {
        console.log(response);
        const responseObj = response.data;
        dispatch(getServicesSuccess(responseObj));
      })
      .catch(error => {
        if (error.response && error.response.status === 400) {
          dispatch(getServicesFail(error.response.data));
        }
      });
  };
};

export const getServiceById = id => {
  return dispatch => {
    dispatch(getServiceByIdRequest());
    axios
      .get(`/stocks/${id}`)
      .then(response => {
        console.log(response);
        const responseObj = response.data;
        dispatch(getServiceByIdSuccess(responseObj));
      })
      .catch(error => {
        if (error.response && error.response.status === 400) {
          dispatch(getServiceByIdFail(error.response.data));
        }
      });
  };
};

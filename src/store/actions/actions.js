import axios from '../../config/axios/axios';
import * as actionTypes from './actionTypes';

const getActionsRequest = () => {
  return {
    type: actionTypes.GET_ACTIONS_REQUEST,
  };
};

const getActionsSuccess = actions => {
  return {
    type: actionTypes.GET_ACTIONS_SUCCESS,
    actions,
  };
};

const getActionsFail = errors => {
  return {
    type: actionTypes.GET_ACTIONS_FAIL,
    errors,
  };
};

const getActionByIdRequest = () => {
  return {
    type: actionTypes.GET_ACTION_BY_ID_REQUEST,
  };
};

const getActionByIdSuccess = action => {
  return {
    type: actionTypes.GET_ACTION_BY_ID_SUCCESS,
    action,
  };
};

const getActionByIdFail = errors => {
  return {
    type: actionTypes.GET_ACTION_BY_ID_FAIL,
    errors,
  };
};

const addActionRequest = () => {
  return {
    type: actionTypes.ADD_ACTION_REQUEST,
  };
};

const addActionSuccess = action => {
  return {
    type: actionTypes.ADD_ACTION_SUCCESS,
    action,
  };
};

const addActionFail = errors => {
  return {
    type: actionTypes.ADD_ACTION_FAIL,
    errors,
  };
};

const updateActionRequest = () => {
  return {
    type: actionTypes.UPDATE_ACTION_REQUEST,
  };
};

const updateActionSuccess = action => {
  return {
    type: actionTypes.UPDATE_ACTION_SUCCESS,
    action,
  };
};

const updateActionFail = errors => {
  return {
    type: actionTypes.UPDATE_ACTION_FAIL,
    errors,
  };
};

const deleteActionRequest = () => {
  return {
    type: actionTypes.DELETE_ACTION_REQUEST,
  };
};

const deleteActionSuccess = id => {
  return {
    type: actionTypes.DELETE_ACTION_SUCCESS,
    id,
  };
};

const deleteActionFail = errors => {
  return {
    type: actionTypes.DELETE_ACTION_FAIL,
    errors,
  };
};

export const getActions = () => {
  return dispatch => {
    dispatch(getActionsRequest());
    axios
      .get('/stocks/')
      .then(response => {
        console.log(response);
        const responseObj = response.data;
        dispatch(getActionsSuccess(responseObj));
      })
      .catch(error => {
        if (error.response && error.response.status === 400) {
          dispatch(getActionsFail(error.response.data));
        }
      });
  };
};

export const getActionById = id => {
  return dispatch => {
    dispatch(getActionByIdRequest());
    axios
      .get(`/stocks/${id}/`)
      .then(response => {
        console.log(response);
        const responseObj = response.data;

        let formattedData = {
          ...responseObj,
        };
        if (responseObj.lifetime_min) {
          const lifetimeMin = +responseObj.lifetime_min.split(' ')[0] / 30;
          formattedData = {
            ...responseObj,
            lifetime_min: lifetimeMin,
          };
        }
        if (responseObj.lifetime_max) {
          const lifetimeMax = +responseObj.lifetime_max.split(' ')[0] / 30;
          formattedData = {
            ...responseObj,
            lifetime_max: lifetimeMax,
          };
        }

        dispatch(getActionByIdSuccess(formattedData));
      })
      .catch(error => {
        if (error.response && error.response.status === 400) {
          dispatch(getActionByIdFail(error.response.data));
        }
      });
  };
};

export const addAction = actionData => {
  const formattedData = {
    ...actionData,
    lifetime_min: actionData.lifetime_min * 2592000,
    lifetime_max: actionData.lifetime_max * 2592000,
  };
  return dispatch => {
    dispatch(addActionRequest());
    axios
      .post('/stocks/', formattedData)
      .then(response => {
        console.log(response);
        const responseObj = response.data;
        dispatch(addActionSuccess(responseObj));
      })
      .catch(error => {
        console.log(error.response);
        if (error.response && error.response.status === 400) {
          dispatch(addActionFail(error.response.data));
        }
      });
  };
};

export const updateAction = (id, actionData) => {
  const formattedData = {
    ...actionData,
    lifetime_min: actionData.lifetime_min * 2592000,
    lifetime_max: actionData.lifetime_max * 2592000,
  };
  return dispatch => {
    dispatch(updateActionRequest());
    axios
      .put(`/stocks/${id}/`, formattedData)
      .then(response => {
        console.log(response);
        const responseObj = response.data;
        dispatch(updateActionSuccess(responseObj));
      })
      .catch(error => {
        console.log(error.response);
        if (error.response && error.response.status === 400) {
          dispatch(updateActionFail(error.response.data));
        }
      });
  };
};

export const deleteAction = id => {
  return dispatch => {
    dispatch(deleteActionRequest());
    axios
      .delete(`/stocks/${id}/`)
      .then(response => {
        console.log(response);
        dispatch(deleteActionSuccess(id));
      })
      .catch(error => {
        if (error.response && error.response.status === 400) {
          dispatch(deleteActionFail(error.response.data));
        }
      });
  };
};

import axios from '../../config/axios/axios';
import * as actionTypes from './actionTypes';

const getDealersRequest = () => {
  return {
    type: actionTypes.GET_DEALERS_REQUEST,
  };
};

const getDealersSuccess = dealers => {
  return {
    type: actionTypes.GET_DEALERS_SUCCESS,
    dealers,
  };
};

const getDealersFail = errors => {
  return {
    type: actionTypes.GET_DEALERS_FAIL,
    errors,
  };
};

const getDealerByIdRequest = () => {
  return {
    type: actionTypes.GET_DEALER_BY_ID_REQUEST,
  };
};

const getDealerByIdSuccess = dealer => {
  return {
    type: actionTypes.GET_DEALER_BY_ID_SUCCESS,
    dealer,
  };
};

const getDealerByIdFail = errors => {
  return {
    type: actionTypes.GET_DEALER_BY_ID_FAIL,
    errors,
  };
};

export const getDealers = () => {
  return dispatch => {
    dispatch(getDealersRequest());
    axios
      .get('/dealers/')
      .then(response => {
        console.log(response);
        const responseObj = response.data;
        dispatch(getDealersSuccess(responseObj));
      })
      .catch(error => {
        if (error.response && error.response.status === 400) {
          dispatch(getDealersFail(error.response.data));
        }
      });
  };
};

export const getDealerById = id => {
  return dispatch => {
    dispatch(getDealerByIdRequest());
    axios
      .get(`/stocks/${id}`)
      .then(response => {
        console.log(response);
        const responseObj = response.data;
        dispatch(getDealerByIdSuccess(responseObj));
      })
      .catch(error => {
        if (error.response && error.response.status === 400) {
          dispatch(getDealerByIdFail(error.response.data));
        }
      });
  };
};

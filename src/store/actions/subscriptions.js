import axios from '../../config/axios/axios';
import * as actionTypes from './actionTypes';

const getSubscriptionsRequest = () => {
  return {
    type: actionTypes.GET_SUBSCRIPTIONS_REQUEST,
  };
};

const getSubscriptionsSuccess = subscriptions => {
  return {
    type: actionTypes.GET_SUBSCRIPTIONS_SUCCESS,
    subscriptions,
  };
};

const getSubscriptionsFail = errors => {
  return {
    type: actionTypes.GET_SUBSCRIPTIONS_FAIL,
    errors,
  };
};

const getSubscriptionByIdRequest = () => {
  return {
    type: actionTypes.GET_SUBSCRIPTION_BY_ID_REQUEST,
  };
};

const getSubscriptionByIdSuccess = subscription => {
  return {
    type: actionTypes.GET_SUBSCRIPTION_BY_ID_SUCCESS,
    subscription,
  };
};

const getSubscriptionByIdFail = errors => {
  return {
    type: actionTypes.GET_SUBSCRIPTION_BY_ID_FAIL,
    errors,
  };
};

export const getSubscriptions = () => {
  return dispatch => {
    dispatch(getSubscriptionsRequest());
    axios
      .get('/subscriptions/')
      .then(response => {
        console.log(response);
        const responseObj = response.data;
        dispatch(getSubscriptionsSuccess(responseObj));
      })
      .catch(error => {
        if (error.response && error.response.status === 400) {
          dispatch(getSubscriptionsFail(error.response.data));
        }
      });
  };
};

export const getSubscriptionById = id => {
  return dispatch => {
    dispatch(getSubscriptionByIdRequest());
    axios
      .get(`/stocks/${id}`)
      .then(response => {
        console.log(response);
        const responseObj = response.data;
        dispatch(getSubscriptionByIdSuccess(responseObj));
      })
      .catch(error => {
        if (error.response && error.response.status === 400) {
          dispatch(getSubscriptionByIdFail(error.response.data));
        }
      });
  };
};

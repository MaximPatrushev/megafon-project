import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import styles from './AddAction.module.scss';
import AddForm from '../../components/AddForm/AddForm';
import AddActionFeedback from '../../components/AddActionFeedback/AddActionFeedback';
import Spinner from '../../components/shared/Spinner/Spinner';
import { addAction, getActions } from '../../store/actions/actions';
import { getServices } from '../../store/actions/services';
import { getSubscriptions } from '../../store/actions/subscriptions';
import { getDealers } from '../../store/actions/dealers';
import CloseImg from '../../assets/images/close.svg';

const AddAction = props => {
  const {
    setModalOpen,
    services,
    subscriptions,
    dealers,
    isLoading,
    errors,
    isServicesLoaded,
    isSubscriptionsLoaded,
    isDealersLoaded,
    isActionAdded,
    getActions,
    getServices,
    getSubscriptions,
    getDealers,
    addAction,
    history,
  } = props;
  console.log(isActionAdded);
  // initial action form state
  const initialState = {
    name: '',
    description: '',
    lifetime_min: 0,
    lifetime_max: 0,
    subscriptions: [],
    balance_min: 0,
    balance_max: 0,
    services: [],
    connected_services: [],
    dealers: [],
  };

  // state for handling adding action form
  const [actionForm, setActionValues] = useState(initialState);

  // fetch services, subscriptions and dealers
  useEffect(() => {
    getServices();
    getSubscriptions();
    getDealers();
    getActions();
  }, []);

  // handling change of action form inputs
  const onInputChange = e => {
    const { id, value } = e.target;
    setActionValues({
      ...actionForm,
      [id]: value,
    });
  };

  const onSelectChange = (options, select) => {
    const id = select.name;
    const values = options.map(option => option.value);
    setActionValues({
      ...actionForm,
      [id]: values,
    });
  };

  // handling post form submit
  const onActionSubmit = e => {
    e.preventDefault();
    addAction(actionForm);
  };

  const goToList = () => {
    setModalOpen(false);
    history.push('/');
  };

  if (!isServicesLoaded || !isSubscriptionsLoaded || !isDealersLoaded) {
    return <Spinner />;
  } else {
    return (
      <div className={styles.container}>
        <div className={styles.containerTopWrap}>
          <h2 className={styles.containerTitle}>Новая акция</h2>
          <button
            className={styles.containerButton}
            type="button"
            onClick={() => setModalOpen(false)}
          >
            <img src={CloseImg} alt="" />
          </button>
        </div>

        {isActionAdded ? (
          <AddActionFeedback goToList={goToList} />
        ) : (
          <AddForm
            form={actionForm}
            services={services}
            subscriptions={subscriptions}
            dealers={dealers}
            isLoading={isLoading}
            errors={errors}
            onInputChange={onInputChange}
            onSelectChange={onSelectChange}
            onActionSubmit={onActionSubmit}
          />
        )}
      </div>
    );
  }
};

const mapStateToProps = ({
  actions: { isLoading, validErrors, isActionAdded },
  services: { services, isServicesLoaded },
  subscriptions: { subscriptions, isSubscriptionsLoaded },
  dealers: { dealers, isDealersLoaded },
}) => {
  return {
    services,
    subscriptions,
    dealers,
    isServicesLoaded,
    isSubscriptionsLoaded,
    isDealersLoaded,
    isLoading,
    isActionAdded,
    errors: validErrors,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addAction: actionData => dispatch(addAction(actionData)),
    getActions: () => dispatch(getActions()),
    getServices: () => dispatch(getServices()),
    getSubscriptions: () => dispatch(getSubscriptions()),
    getDealers: () => dispatch(getDealers()),
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(AddAction),
);

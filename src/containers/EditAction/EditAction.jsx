import React, { useEffect, useState } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import styles from './EditAction.module.scss';

import UpdateActionFeedback from '../../components/UpdateActionFeedback/UpdateActionFeedback';
import EditForm from '../../components/EditForm/EditForm';
import Spinner from '../../components/shared/Spinner/Spinner';
import {
  deleteAction,
  getActionById,
  updateAction,
} from '../../store/actions/actions';
import { getServices } from '../../store/actions/services';
import { getSubscriptions } from '../../store/actions/subscriptions';
import { getDealers } from '../../store/actions/dealers';
import CloseImg from '../../assets/images/close.svg';

const EditAction = props => {
  const {
    action,
    actionId,
    setModalOpen,
    services,
    subscriptions,
    dealers,
    isLoading,
    errors,
    isServicesLoaded,
    isSubscriptionsLoaded,
    isDealersLoaded,
    getServices,
    getSubscriptions,
    getDealers,
    getActionById,
    updateAction,
    deleteAction,
    isActionUpdated,
    isActionDeleted,
    isActionLoaded,
    history,
  } = props;
  console.log('action', action);

  // state for handling adding action form
  const [actionForm, setActionValues] = useState(action);

  // fetch services, subscriptions and dealers
  useEffect(() => {
    getActionById(actionId);
    getServices();
    getSubscriptions();
    getDealers();
  }, []);

  useEffect(() => {
    setActionValues(action);
  }, [action.name]);

  // handling change of action form inputs
  const onInputChange = e => {
    const { id, value } = e.target;
    setActionValues({
      ...actionForm,
      [id]: value,
    });
  };

  const onSelectChange = (options, select) => {
    const id = select.name;
    const values = options.map(option => option.value);
    setActionValues({
      ...actionForm,
      [id]: values,
    });
  };

  // handling post form submit
  const onActionSubmit = e => {
    e.preventDefault();
    updateAction(actionId, actionForm);
  };

  const onDeleteAction = id => {
    deleteAction(id);
    setModalOpen(false);
  };

  const goToList = () => {
    setModalOpen(false);
    history.push('/');
  };

  if (isActionDeleted) {
    return <Redirect to="/" />;
  }

  if (
    !isServicesLoaded ||
    !isSubscriptionsLoaded ||
    !isDealersLoaded ||
    !isActionLoaded
  ) {
    return <Spinner />;
  } else {
    return (
      <div className={styles.container}>
        <div className={styles.containerTopWrap}>
          <h2 className={styles.containerTitle}>Настройки акции</h2>
          <button
            className={styles.containerButton}
            type="button"
            onClick={() => setModalOpen(false)}
          >
            <img src={CloseImg} alt="" />
          </button>
        </div>

        {isActionUpdated ? (
          <UpdateActionFeedback goToList={goToList} />
        ) : (
          <EditForm
            actionId={actionId}
            form={actionForm}
            services={services}
            subscriptions={subscriptions}
            dealers={dealers}
            isLoading={isLoading}
            errors={errors}
            onInputChange={onInputChange}
            onSelectChange={onSelectChange}
            onActionSubmit={onActionSubmit}
            onDeleteAction={onDeleteAction}
          />
        )}
      </div>
    );
  }
};

const mapStateToProps = ({
  actions: {
    action,
    isActionsLoaded,
    isLoading,
    validErrors,
    isActionUpdated,
    isActionDeleted,
    isActionLoaded,
  },
  services: { services, isServicesLoaded },
  subscriptions: { subscriptions, isSubscriptionsLoaded },
  dealers: { dealers, isDealersLoaded },
}) => {
  return {
    action,
    services,
    subscriptions,
    dealers,
    isServicesLoaded,
    isSubscriptionsLoaded,
    isDealersLoaded,
    isLoading,
    isActionsLoaded,
    isActionUpdated,
    isActionDeleted,
    isActionLoaded,
    errors: validErrors,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getActionById: id => dispatch(getActionById(id)),
    updateAction: (id, actionData) => dispatch(updateAction(id, actionData)),
    deleteAction: id => dispatch(deleteAction(id)),
    getServices: () => dispatch(getServices()),
    getSubscriptions: () => dispatch(getSubscriptions()),
    getDealers: () => dispatch(getDealers()),
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(EditAction),
);

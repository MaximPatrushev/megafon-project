import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import SearchForm from '../../components/SearchForm/SearchForm';
import ActionCard from '../../components/ActionCard/ActionCard';
import Spinner from '../../components/shared/Spinner/Spinner';
import styles from './ActionsList.module.scss';
import { getActions } from '../../store/actions/actions';
import { getServices } from '../../store/actions/services';
import { getFilteredServices } from '../../store/selectors';

const ActionList = props => {
  const {
    actions,
    services,
    isActionsLoaded,
    isServicesLoaded,
    getActions,
    getServices,
    getFilteredServices,
    handleEditModalOpen,
  } = props;

  // fetch actions on component load
  useEffect(() => {
    getActions();
    getServices();
  }, []);

  const renderActionCards = actions => {
    return actions.map(action => {
      return (
        <ActionCard
          action={action}
          key={action.id}
          services={getFilteredServices({ action, services })}
          handleEditModalOpen={handleEditModalOpen}
        />
      );
    });
  };

  if (!isActionsLoaded || !isServicesLoaded) {
    return <Spinner />;
  } else {
    return (
      <div className={styles.container}>
        <h1 className={styles.title}>Список акций</h1>
        <SearchForm />

        {renderActionCards(actions)}
      </div>
    );
  }
};

const mapStateToProps = ({
  actions: { actions, isActionsLoaded, validErrors },
  services: { services, isServicesLoaded },
}) => {
  return {
    actions,
    services,
    isActionsLoaded,
    isServicesLoaded,
    errors: validErrors,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getActions: () => dispatch(getActions()),
    getServices: () => dispatch(getServices()),
    getFilteredServices: state => getFilteredServices(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ActionList);

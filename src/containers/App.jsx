import React, { Fragment, useState } from 'react';
import { Route, Switch } from 'react-router-dom';
import Modal from 'react-modal';

import ActionsList from '../containers/ActionsList/ActionsList';
import ActionDetails from '../containers/ActionDetails/ActionDetails';
import withErrorHandler from '../hoc/withErrorHandler';
import NotFound from '../components/shared/NotFound/NotFound';
import Header from '../components/Header/Header';
import AddAction from './AddAction/AddAction';
import EditAction from './EditAction/EditAction';

//setting parent node for modal window
Modal.setAppElement('#root');

const App = () => {
  // state for handling add action modal window
  const [isAddModalOpen, setAddModalOpen] = useState(false);
  // state for handling edit action modal window
  const [editModal, handleEditModalOpen] = useState({
    isOpen: false,
    actionId: null,
  });

  const setEditModalOpen = value => {
    handleEditModalOpen({
      ...editModal,
      isOpen: value,
    });
  };

  return (
    <Fragment>
      <Header setModalOpen={setAddModalOpen} />

      <Switch>
        <Route
          path="/"
          exact
          render={() => {
            return <ActionsList handleEditModalOpen={handleEditModalOpen} />;
          }}
        />
        <Route
          path="/action/:id"
          render={({ match }) => {
            return (
              <ActionDetails
                match={match}
                handleEditModalOpen={handleEditModalOpen}
              />
            );
          }}
        />
        <Route component={NotFound} />
      </Switch>

      <Modal
        isOpen={isAddModalOpen}
        onRequestClose={() => setAddModalOpen(false)}
        className="add-action"
        overlayClassName="overlay"
        shouldCloseOnOverlayClick={true}
        closeTimeoutMS={300}
      >
        <AddAction setModalOpen={setAddModalOpen} />
      </Modal>

      <Modal
        isOpen={editModal.isOpen}
        onRequestClose={() => setEditModalOpen(false)}
        className="add-action"
        overlayClassName="overlay"
        shouldCloseOnOverlayClick={true}
        closeTimeoutMS={300}
      >
        <EditAction
          setModalOpen={setEditModalOpen}
          actionId={editModal.actionId}
        />
      </Modal>
    </Fragment>
  );
};

export default withErrorHandler(App);

import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import ActionHeader from '../../components/ActionDetails/ActionHeader/ActionHeader';
import ActionConnectedServices from '../../components/ActionDetails/ActionConnectedServices/ActionConnectedServices';
import ActionSubscriptions from '../../components/ActionDetails/ActionSubscriptions/ActionSubscriptions';
import ActionServices from '../../components/ActionDetails/ActionServices/ActionServices';
import ActionBalance from '../../components/ActionDetails/ActionBalance/ActionBalance';
import ActionLifetime from '../../components/ActionDetails/ActionLifetime/ActionLifetime';
import ActionDescription from '../../components/ActionDetails/ActionDescription/ActionDescription';
import Spinner from '../../components/shared/Spinner/Spinner';
import styles from './ActionDetails.module.scss';
import { getActionById } from '../../store/actions/actions';
import { getServices } from '../../store/actions/services';
import { getSubscriptions } from '../../store/actions/subscriptions';
import {
  getFilteredServices,
  getFilteredConnectedServices,
  getFilteredSubscriptions,
} from '../../store/selectors';

const ActionDetails = props => {
  const {
    action,
    services,
    connectedServices,
    subscriptions,
    isActionsLoaded,
    isServicesLoaded,
    isSubscriptionsLoaded,
    getActionById,
    getServices,
    getSubscriptions,
    handleEditModalOpen,
    match,
  } = props;

  // id of current action
  const actionId = match.params.id;

  // fetch action, services and subscriptions
  useEffect(() => {
    getActionById(actionId);
    getServices();
    getSubscriptions();
  }, []);

  if (!isActionsLoaded || !isServicesLoaded || !isSubscriptionsLoaded) {
    return <Spinner />;
  } else {
    return (
      <div className={styles.container}>
        <ActionHeader
          action={action}
          handleEditModalOpen={handleEditModalOpen}
        />

        <div className={styles.infoContainer}>
          <div className={styles.escapeWrap}>
            <ActionConnectedServices connectedServices={connectedServices} />

            <div className={styles.userInfo}>
              <p className={styles.userInfoTitle}>Требования к абоненту</p>

              <div className={styles.userInfoEscape}>
                {!!subscriptions.length && (
                  <ActionSubscriptions subscriptions={subscriptions} />
                )}

                {!!services.length && <ActionServices services={services} />}

                {(!!action.balance_min || !!action.balance_max) && (
                  <ActionBalance action={action} />
                )}

                {(!!action.lifetime_min || !!action.lifetime_max) && (
                  <ActionLifetime action={action} />
                )}
              </div>
            </div>
          </div>

          <ActionDescription description={action.description} />
        </div>
      </div>
    );
  }
};

const mapStateToProps = ({
  actions: { action, isActionsLoaded, validErrors },
  services: { services, isServicesLoaded },
  subscriptions: { subscriptions, isSubscriptionsLoaded },
}) => {
  return {
    action,
    services: getFilteredServices({ action, services }),
    connectedServices: getFilteredConnectedServices({ action, services }),
    subscriptions: getFilteredSubscriptions({ action, subscriptions }),
    isActionsLoaded,
    isServicesLoaded,
    isSubscriptionsLoaded,
    errors: validErrors,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getActionById: id => dispatch(getActionById(id)),
    getServices: () => dispatch(getServices()),
    getSubscriptions: () => dispatch(getSubscriptions()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ActionDetails);

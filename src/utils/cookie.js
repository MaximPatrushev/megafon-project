export const setCookie = (key, value, time) => {
  const d = new Date();
  let expires = '';
  // if time was set, the const cookie with expiration time, else session cookie
  if (time) {
    d.setTime(d.getTime() + time);
    expires = 'expires=' + d.toUTCString();
  }
  document.cookie = key + '=' + value + ';' + expires + ';path=/';
};

export const getCookie = key => {
  const name = key + '=';
  const decodedCookie = decodeURIComponent(document.cookie);
  const cookieArr = decodedCookie.split(';');
  for (let i = 0; i < cookieArr.length; i++) {
    let c = cookieArr[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return null;
};

export const clearCookies = () => {
  const cookies = document.cookie.split(';');
  for (let i = 0; i < cookies.length; i++) {
    const cookie = cookies[i];
    const eqPos = cookie.indexOf('=');
    const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/';
  }
};

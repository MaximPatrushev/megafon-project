import React from 'react';

import CircleImg from '../../../assets/images/Spinner.svg';
import styles from './spinner.module.scss';

const Spinner = () => {
  return (
    <img
      className={styles.loader}
      width="100px"
      height="100px"
      src={CircleImg}
      alt="Loading..."
    />
  );
};

export default Spinner;

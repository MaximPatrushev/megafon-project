import React from 'react';

import styles from './NotFound.module.css';

const NotFound = ({ history }) => {
  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <h1 className={styles.title}>Страница не найдена</h1>
      </div>
      <button className={styles.linkBack} onClick={() => history.goBack()}>
        Назад
      </button>
    </div>
  );
};

export default NotFound;

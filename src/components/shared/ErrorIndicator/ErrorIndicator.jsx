import React from 'react';
import { Link } from 'react-router-dom';

import styles from './ErrorIndicator.module.css';

const ErrorIndicator = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Похоже, что-то пошло не так</h1>
      <p className={styles.text}>
        Попробуйте перезагрузить страницу или вернить на главную
      </p>
      <Link className={styles.link} to="/">
        Перейти на главную
      </Link>
    </div>
  );
};

export default ErrorIndicator;

import React from 'react';
import { Link } from 'react-router-dom';
import GearImg from '../../assets/images/gear.svg';
import styles from './ActionCard.module.scss';

const ActionCard = ({ action, services, handleEditModalOpen }) => {
  const renderServices = services => {
    return services.map(service => {
      return (
        <li className={styles.cardInfoUnit} key={service.id}>
          {service.name}
        </li>
      );
    });
  };

  return (
    <div className={styles.card}>
      <div className={styles.cardTitleWrapper}>
        <Link to={`/action/${action.id}`}>
          <h2 className={styles.cardTitle}>{action.name}</h2>
        </Link>
      </div>
      <div className={styles.cardInfo}>
        <ul>{renderServices(services)}</ul>
      </div>
      <div
        className={styles.cardEdit}
        onClick={() =>
          handleEditModalOpen({
            isOpen: true,
            actionId: action.id,
          })
        }
      >
        <img className={styles.cardEditImg} src={GearImg} alt="" />
        <p className={styles.cardEditText}>Настроить акцию</p>
      </div>
    </div>
  );
};

export default ActionCard;

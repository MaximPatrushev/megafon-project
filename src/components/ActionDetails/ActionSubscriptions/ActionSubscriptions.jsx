import React from 'react';

import styles from './ActionSubscriptions.module.scss';

const ActionSubscriptions = ({ subscriptions }) => {
  return (
    <div className={styles.container}>
      <h2 className={styles.containerTitle}>Тарифы</h2>
      <ul>
        {subscriptions.map(subscription => {
          return (
            <li className={styles.containerListUnit} key={subscription.id}>
              {subscription.name}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default ActionSubscriptions;

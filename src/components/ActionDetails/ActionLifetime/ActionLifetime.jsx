import React from 'react';

import styles from './ActionHeader.module.scss';

const ActionHeader = ({ action }) => {
  return (
    <div className={styles.container}>
      <h2 className={styles.containerTitle}>Абонент в компании</h2>
      <ul>
        <p className={styles.containerListUnit}>
          {action.lifetime_min}-{action.lifetime_max}
        </p>
      </ul>
    </div>
  );
};

export default ActionHeader;

import React from 'react';

import styles from './ActionServices.module.scss';

const ActionServices = ({ services }) => {
  return (
    <div className={styles.container}>
      <h2 className={styles.containerTitle}>Услуги</h2>
      <ul>
        {services.map(service => {
          return (
            <li className={styles.containerListUnit} key={service.id}>
              {service.name}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default ActionServices;

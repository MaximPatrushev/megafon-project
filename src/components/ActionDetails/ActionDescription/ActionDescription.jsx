import React from 'react';

import styles from './ActionDescription.module.scss';

const ActionDescription = ({ description }) => {
  return (
    <div className={styles.container}>
      <h2 className={styles.containerTitle}>Описание</h2>
      <p className={styles.containerDescription}>{description}</p>
    </div>
  );
};

export default ActionDescription;

import React from 'react';
import { Link } from 'react-router-dom';
import GearImg from '../../../assets/images/gear.svg';
import styles from './ActionHeader.module.scss';

const ActionHeader = ({ action, handleEditModalOpen }) => {
  return (
    <div className={styles.main}>
      <div className={styles.container}>
        <Link to="/">Акции</Link>
        <p className={styles.space}>-</p>
        <Link to={`/action/${action.id}`}>{action.name}</Link>
      </div>
      <div className={styles.wrapper}>
        <h1 className={styles.wrapperTitle}>{action.name}</h1>
        <p
          className={styles.AddAction}
          onClick={() =>
            handleEditModalOpen({
              isOpen: true,
              actionId: action.id,
            })
          }
        >
          <img className={styles.AddActionImg} src={GearImg} alt="" />
          Настроить акцию
        </p>
      </div>
    </div>
  );
};

export default ActionHeader;

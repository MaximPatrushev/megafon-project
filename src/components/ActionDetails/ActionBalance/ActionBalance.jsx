import React from 'react';

import styles from './ActionBalance.module.scss';

const ActionBalance = ({ action }) => {
  return (
    <div className={styles.container}>
      <h2 className={styles.containerTitle}>Баланс</h2>
      <ul>
        <p className={styles.containerListUnit}>
          {action.balance_min}-{action.balance_max}
        </p>
      </ul>
    </div>
  );
};

export default ActionBalance;

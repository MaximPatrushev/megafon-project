import React from 'react';

import styles from './ActionConnectedServices.module.scss';

const ActionConnectedServices = ({ connectedServices }) => {
  return (
    <div className={styles.container}>
      <h2 className={styles.containerTitle}>Подключаемые услуги</h2>
      <ul>
        {connectedServices.map(service => {
          return (
            <li className={styles.containerListUnit} key={service.id}>
              {service.name}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default ActionConnectedServices;

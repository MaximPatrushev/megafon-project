import React, { useState } from 'react';
import Select from 'react-select';

import styles from './EditForm.module.scss';
import SubscriptionsInput from '../AddForm/SubscriptionsInput/SubscriptionsInput';
import ServicesInput from '../AddForm/ServicesInput/ServicesInput';
import DealersInput from '../AddForm/DealersInput/DealersInput';
import BalanceInput from '../AddForm/BalanceInput/BalanceInput';
import LifetimeInput from '../AddForm/LifetimeInput/LifetimeInput';
import Dropdown from '../AddForm/Dropdown/Dropdown';
import Spinner from '../shared/Spinner/Spinner';

const EditForm = props => {
  const {
    actionId,
    form,
    onInputChange,
    onSelectChange,
    onActionSubmit,
    services,
    subscriptions,
    dealers,
    isLoading,
    errors,
    onDeleteAction,
  } = props;
  console.log(form);
  // handling state of conditions dropdown
  const [isDropdownOpen, setDropdownOpen] = useState(false);

  //handling state of showing condition inputs
  const [isConditionsShow, setConditionsShow] = useState({
    SubscriptionsInput: false,
    ServicesInput: false,
    DealersInput: false,
    BalanceInput: false,
    LifetimeInput: false,
  });

  const conditions = [
    { id: 1, name: 'Тарифы', component: 'SubscriptionsInput' },
    { id: 2, name: 'Услуги', component: 'ServicesInput' },
    { id: 3, name: 'Партнеры', component: 'DealersInput' },
    { id: 4, name: 'Баланс', component: 'BalanceInput' },
    { id: 5, name: 'Время абонента в компании', component: 'LifetimeInput' },
  ];

  const showInput = component => {
    setDropdownOpen(false);
    setConditionsShow({
      ...isConditionsShow,
      [component]: true,
    });
  };

  const deleteInput = component => {
    console.log('component', component);
    setConditionsShow({
      ...isConditionsShow,
      [component]: false,
    });
  };

  const serviceOptions = services.map(service => {
    return { value: service.id, label: service.name };
  });

  const subscriptionOptions = subscriptions.map(subscription => {
    return { value: subscription.id, label: subscription.name };
  });

  const dealerOptions = dealers.map(dealer => {
    return { value: dealer.id, label: dealer.name };
  });

  return (
    <form onSubmit={e => onActionSubmit(e)}>
      <div className={styles.container}>
        <div className={styles.formContainer}>
          <h3 className={styles.formContainerTitle}>Параметры акции</h3>

          <div className={styles.formGroup}>
            <label className={styles.formGroupTitle} htmlFor="name">
              Название акции
            </label>
            <input
              type="text"
              className={styles.formControl}
              id="name"
              value={form.name}
              onChange={e => onInputChange(e)}
            />
            {errors && errors.name && (
              <p className={styles.errorText}>Это поле не может быть пустым</p>
            )}
          </div>

          <div className={styles.formGroup}>
            <label
              className={styles.formGroupTitle}
              htmlFor="connected_services"
            >
              Подключаемые услуги
            </label>
            <Select
              id="connected_services"
              name="connected_services"
              onChange={(options, select) => onSelectChange(options, select)}
              options={serviceOptions}
              isMulti={true}
              placeholder="Выберите услуги"
            />
          </div>

          <div className={styles.formGroup}>
            <label className={styles.formGroupTitle} htmlFor="description">
              Описание
            </label>
            <textarea
              className="form-control"
              id="description"
              rows="5"
              value={form.description}
              onChange={e => onInputChange(e)}
            />
            {errors && errors.description && (
              <p className={styles.errorText}>Это поле не может быть пустым</p>
            )}
          </div>
        </div>

        <div className={styles.formContainer}>
          <h3 className={styles.formContainerTitle}>Требования к абоненту</h3>
          <p
            onClick={() => setDropdownOpen(true)}
            className={styles.formContainerAdd}
          >
            + Добавить условие
          </p>
          {isDropdownOpen && (
            <Dropdown
              conditions={conditions}
              showInput={showInput}
              setDropdownOpen={setDropdownOpen}
            />
          )}
          {(isConditionsShow.SubscriptionsInput ||
            (form.subscriptions && !!form.subscriptions.length)) && (
              <SubscriptionsInput
                subscriptionOptions={subscriptionOptions}
                onSelectChange={onSelectChange}
                deleteInput={deleteInput}
                component="SubscriptionsInput"
                subscriptions={form.subscriptions}
              />
            )}
          {(isConditionsShow.ServicesInput || (form.services && !!form.services.length)) && (
            <ServicesInput
              serviceOptions={serviceOptions}
              onSelectChange={onSelectChange}
              deleteInput={deleteInput}
              component="ServicesInput"
              services={form.services}
            />
          )}
          {(isConditionsShow.DealersInput || (form.dealers && !!form.dealers.length)) && (
            <DealersInput
              dealerOptions={dealerOptions}
              onSelectChange={onSelectChange}
              deleteInput={deleteInput}
              component="DealersInput"
              dealers={form.dealers}
            />
          )}
          {(isConditionsShow.BalanceInput ||
            (!!form.balance_min || !!form.balance_max)) && (
              <BalanceInput
                form={form}
                onInputChange={onInputChange}
                deleteInput={deleteInput}
                component="BalanceInput"
              />
            )}
          {(isConditionsShow.LifetimeInput ||
            (!!form.lifetime_min || !!form.lifetime_max)) && (
              <LifetimeInput
                form={form}
                onInputChange={onInputChange}
                deleteInput={deleteInput}
                component="LifetimeInput"
              />
            )}
          {errors && errors.conditions && (
            <p className={styles.errorText}>Должно быть указано хотя бы одно условие для подключения тарифа</p>
          )}
        </div>
      </div>
      <div className={styles.formLine}>
        {isLoading ? (
          <Spinner />
        ) : (
          <button className={styles.ButtonFormAddAction} type="submit">
            Сохранить изменения
          </button>
        )}

        <p
          className={styles.ButtonFormAddAction}
          onClick={() => onDeleteAction(actionId)}
        >
          Удалить акцию
        </p>
      </div>
    </form>
  );
};

export default EditForm;

import React from 'react';
import onClickOutside from 'react-onclickoutside';

const Dropdown = ({ conditions, showInput, setDropdownOpen }) => {
  Dropdown.handleClickOutside = () => {
    setDropdownOpen(false);
  };

  return (
    <ul>
      {conditions.map(condition => {
        return (
          <li key={condition.id} onClick={() => showInput(condition.component)}>
            {condition.name}
          </li>
        );
      })}
    </ul>
  );
};

const clickOutsideConfig = {
  handleClickOutside: () => Dropdown.handleClickOutside,
};

export default onClickOutside(Dropdown, clickOutsideConfig);

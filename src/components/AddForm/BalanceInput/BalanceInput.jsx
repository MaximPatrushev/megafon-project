import React, { Fragment } from 'react';

import styles from './BalanceInput.module.scss';

const BalanceInput = props => {
  const { form, onInputChange, deleteInput, component, errors } = props;
  return (
    <Fragment>
      <div className={styles.formGroup}>
        <div className={styles.formGroupEscape}>
          <label className={styles.labelTitle} htmlFor="balance_min">
            Баланс
          </label>
          <p
            className={styles.formDelete}
            onClick={() => deleteInput(component)}
          >
            Удалить
          </p>
        </div>
        <div className={styles.inputWrapper}>
          <input
            type="text"
            className={styles.formControlInput}
            id="balance_min"
            value={form.balance_min}
            onChange={e => onInputChange(e)}
          />
          <input
            type="text"
            className={styles.formControlInput}
            id="balance_max"
            value={form.balance_max}
            onChange={e => onInputChange(e)}
          />
        </div>
        {errors && (errors.balance_max || errors.balance_min) && (
          <p className={styles.errorText}>Требуется целочисленное значение</p>
        )}
      </div>
    </Fragment>
  );
};

export default BalanceInput;

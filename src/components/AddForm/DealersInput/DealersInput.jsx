import React, { Fragment } from 'react';
import Select from 'react-select';

import styles from './DealersInput.module.scss';

const DealersInput = props => {
  const {
    onSelectChange,
    dealerOptions,
    deleteInput,
    component,
    dealers,
  } = props;
  return (
    <Fragment>
      <div className={styles.formGroup}>
        <div className={styles.formGroupEscape}>
          <label className={styles.labelTitle} htmlFor="dealers">
            Партнеры
          </label>
          <p
            className={styles.formDelete}
            onClick={() => deleteInput(component)}
          >
            Удалить
          </p>
        </div>
        <Select
          defaultValue={dealers || []}
          id="dealers"
          name="dealers"
          onChange={(options, select) => onSelectChange(options, select)}
          options={dealerOptions}
          isMulti={true}
        />
      </div>
    </Fragment>
  );
};

export default DealersInput;

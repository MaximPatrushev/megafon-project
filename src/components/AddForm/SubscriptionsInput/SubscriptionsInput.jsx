import React, { Fragment } from 'react';
import Select from 'react-select';

import styles from './SuscriptionsInput.module.scss';

const SubscriptionsInput = props => {
  const {
    onSelectChange,
    subscriptionOptions,
    deleteInput,
    component,
    subscriptions,
  } = props;
  return (
    <Fragment>
      <div className={styles.formGroup}>
        <div className={styles.formGroupEscape}>
          <label className={styles.labelTitle} htmlFor="subscriptions">
            Тарифы
          </label>
          <p
            className={styles.formDelete}
            onClick={() => deleteInput(component)}
          >
            Удалить
          </p>
        </div>
        <Select
          defaultValue={subscriptions || []}
          id="subscriptions"
          name="subscriptions"
          onChange={(options, select) => onSelectChange(options, select)}
          options={subscriptionOptions}
          isMulti={true}
          placeholder="Выберите тарифы"
        />
      </div>
    </Fragment>
  );
};

export default SubscriptionsInput;

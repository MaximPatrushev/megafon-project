import React, { Fragment } from 'react';
import Select from 'react-select';

import styles from './SuscriptionsInput.module.scss';

const ServicesInput = props => {
  const {
    onSelectChange,
    serviceOptions,
    deleteInput,
    component,
    services,
  } = props;
  return (
    <Fragment>
      <div className={styles.formGroup}>
        <div className={styles.formGroupEscape}>
          <label className={styles.labelTitle} htmlFor="services">
            Услуги
          </label>
          <p
            className={styles.formDelete}
            onClick={() => deleteInput(component)}
          >
            Удалить
          </p>
        </div>
        <Select
          defaultValue={services || []}
          id="services"
          name="services"
          onChange={(options, select) => onSelectChange(options, select)}
          options={serviceOptions}
          isMulti={true}
          placeholder="Выберите услуги"
        />
      </div>
    </Fragment>
  );
};

export default ServicesInput;

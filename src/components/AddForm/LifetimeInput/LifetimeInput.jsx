import React, { Fragment } from 'react';

import styles from './LifeTimeInput.module.scss';

const LifetimeInput = props => {
  const { form, onInputChange, deleteInput, component } = props;
  return (
    <Fragment>
      <div className={styles.formGroup}>
        <div className={styles.formGroupEscape}>
          <label className={styles.labelTitle} htmlFor="lifetime_min">
            Абонент в компании
          </label>
          <p
            className={styles.formDelete}
            onClick={() => deleteInput(component)}
          >
            Удалить
          </p>
        </div>
        <input
          type="text"
          className={styles.formControl}
          id="lifetime_min"
          value={form.lifetime_min}
          onChange={e => onInputChange(e)}
        />
        <input
          type="text"
          className={styles.formControl}
          id="lifetime_max"
          value={form.lifetime_max}
          onChange={e => onInputChange(e)}
        />
      </div>
    </Fragment>
  );
};

export default LifetimeInput;

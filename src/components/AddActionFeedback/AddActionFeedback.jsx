import React from 'react';
import styles from './AddActionFeedback.module.scss';
import YepImg from '../../assets/images/yep.svg';

const AddActionFeedback = ({ goToList }) => {
  return (
    <div className={styles.container}>
      <div className={styles.containerInfo}>
        <div className={styles.circle}>
          <img src={YepImg} alt="" />
        </div>
        <h2 className={styles.containerTitle}>Акция успешно добавлена</h2>
        <button
          className={styles.containerButton}
          type="button"
          onClick={() => goToList()}
        >
          К списку акций
        </button>
      </div>
    </div>
  );
};

export default AddActionFeedback;

import React from 'react';

import styles from './SearchForm.module.scss';

const SearchForm = () => {
  return (
    <div>
      <form className={styles.form}>
        <input
          className={styles.search}
          type="text"
          placeholder="Введите название акции"
        />
      </form>
    </div>
  );
};

export default SearchForm;

import React from 'react';
import { Link } from 'react-router-dom';
import LogoImg from '../../assets/images/logo.svg';
import PlusImg from '../../assets/images/plus.svg';
import styles from './Header.module.scss';

const Header = ({ setModalOpen }) => {
  return (
    <header className={styles.headerLogo}>
      <div className={styles.container}>
        <div className={styles.logoWrap}>
          <Link to="/">
            <img className={styles.logo} src={LogoImg} alt="" />
          </Link>
        </div>
        <button
          className={styles.headerButton}
          type="button"
          onClick={() => setModalOpen(true)}
        >
          <img className={styles.headerButtonImg} src={PlusImg} alt="" />
          Добавить акцию
        </button>
      </div>
    </header>
  );
};

export default Header;
